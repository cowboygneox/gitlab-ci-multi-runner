#!/bin/bash

if [ ! -e /etc/gitlab-runner/config.toml && ${REGISTRATION_TOKEN} && ${CI_SERVER_URL} ]
then
  export RUNNER_EXECUTOR=shell; /usr/bin/gitlab-runner register -n
fi

/usr/bin/gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner
